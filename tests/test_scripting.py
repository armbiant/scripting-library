"""SDP Scripting library tests."""

# pylint: disable=redefined-outer-name
# pylint: disable=duplicate-code
# pylint: disable=invalid-name
# pylint: disable=too-many-locals
# pylint: disable=no-value-for-parameter

import json
import logging
import os
from copy import deepcopy
from unittest.mock import patch

import ska_sdp_config
import yaml

from ska_sdp_scripting import ProcessingBlock, new_config_client

LOG = logging.getLogger("scripting-test")
LOG.setLevel(logging.DEBUG)

CONFIG_DB_CLIENT = new_config_client()
SUBARRAY_ID = "01"
MOCK_ENV_VARS = {"SDP_HELM_NAMESPACE": "sdp"}

SCAN_TYPES = [
    {
        "scan_type_id": "target:a",
        "derive_from": ".default",
        "beams": {
            "vis0": {
                "field_id": "field_a",
                "channels_id": "vis_channels",
                "polarisations_id": "all",
                "spectral_windows": [
                    {
                        "spectral_window_id": "fsp_1_channels",
                        "count": 4,
                        "start": 0,
                        "stride": 2,
                        "freq_min": 350000000,
                        "freq_max": 368000000,
                        "link_map": [[0, 0], [200, 1], [744, 2], [944, 3]],
                    },
                    {
                        "spectral_window_id": "fsp_2_channels",
                        "count": 7,
                        "start": 2000,
                        "stride": 1,
                        "freq_min": 360000000,
                        "freq_max": 368000000,
                        "link_map": [[2000, 4], [2200, 5]],
                    },
                ],
            }
        },
    },
    {
        "scan_type_id": "target:b",
        "derive_from": ".default",
        "beams": {
            "vis0": {
                "field_id": "field_a",
                "channels_id": "vis_channels",
                "polarisations_id": "all",
                "spectral_windows": [
                    {
                        "spectral_window_id": "fsp_1_channels",
                        "count": 4,
                        "start": 0,
                        "stride": 2,
                        "freq_min": 350000000,
                        "freq_max": 368000000,
                        "link_map": [[0, 0], [200, 1], [744, 2], [944, 3]],
                    },
                    {
                        "spectral_window_id": "fsp_2_channels",
                        "count": 4,
                        "start": 2000,
                        "stride": 1,
                        "freq_min": 360000000,
                        "freq_max": 368000000,
                        "link_map": [[2000, 4], [2200, 5]],
                    },
                ],
            }
        },
    },
]


# receive addresses when SCAN_TYPE global variable is used
RECV_ADDRESS = {
    "target:a": {
        "vis0": {
            "host": [
                [
                    0,
                    "proc-pb-test-20200425-00000-test-receive-0."
                    "receive.sdp.svc.cluster.local",
                ],
                [
                    2000,
                    "proc-pb-test-20200425-00000-test-receive-1."
                    "receive.sdp.svc.cluster.local",
                ],
            ],
            "port": [[0, 9000, 1], [2000, 9000, 1]],
            "function": "visibilities",
        }
    },
    "target:b": {
        "vis0": {
            "host": [
                [
                    0,
                    "proc-pb-test-20200425-00000-test-receive-0."
                    "receive.sdp.svc.cluster.local",
                ],
                [
                    2000,
                    "proc-pb-test-20200425-00000-test-receive-1."
                    "receive.sdp.svc.cluster.local",
                ],
            ],
            "port": [[0, 9000, 1], [2000, 9000, 1]],
            "function": "visibilities",
        }
    },
}

VALUES = {
    "reception.receiver_port_start": "9000",
    "reception.num_ports": 1,
}


# These are the receive addresses generated when
# the configuration_string.json file is used for scan_types
RECV_ADDR_FROM_CONFIG_STRING = {
    "target:a": {
        "vis0": {
            "host": [
                [
                    0,
                    "proc-pb-test-20200425-00000-test-receive-0."
                    "receive.sdp.svc.cluster.local",
                ]
            ],
            "port": [[0, 9000, 1]],
            "function": "visibilities",
        },
        "pss1": {
            "host": [
                [
                    0,
                    "proc-pb-test-20200425-00000-test-receive-0."
                    "receive.sdp.svc.cluster.local",
                ]
            ],
            "port": [[0, 9000, 1]],
            "function": "pulsar search",
            "search_beam_id": 1,
        },
    }
}


def test_claim_processing_block():
    """Test claiming processing block"""

    # Wipe the config DB
    wipe_config_db()

    # Create eb and pb
    create_eb_pb()

    for txn in CONFIG_DB_CLIENT.txn():
        pb_list = txn.list_processing_blocks()
        for pb_id in pb_list:
            assert txn.get_processing_block(pb_id).pb_id == pb_id
            ProcessingBlock(pb_id)
            assert txn.is_processing_block_owner(pb_id)


def test_buffer_request():
    """Test requesting input and output buffer."""

    # Wipe the config DB
    wipe_config_db()

    # Create eb and pb
    create_eb_pb()

    for txn in CONFIG_DB_CLIENT.txn():
        pb_list = txn.list_processing_blocks()
        for pb_id in pb_list:
            pb = ProcessingBlock(pb_id)
            parameters = pb.get_parameters()
            assert parameters["length"] == 10
            in_buffer_res = pb.request_buffer(100e6, tags=["sdm"])
            out_buffer_res = pb.request_buffer(
                parameters["length"] * 6e15 / 3600, tags=["visibilities"]
            )
            assert in_buffer_res is not None
            assert out_buffer_res is not None


def test_real_time_script():
    """Test real time script."""

    # Wipe the config DB
    wipe_config_db()

    # Create eb and pb
    create_eb_pb()

    # Create processing block states
    create_pb_states()

    pb_id = "pb-test-20200425-00001"
    deploy_name = "cbf-sdp-emulator"
    deploy_id = f"proc-{pb_id}-{deploy_name}"
    work_phase = create_work_phase(pb_id)

    with work_phase:
        for txn in CONFIG_DB_CLIENT.txn():
            eb_list = txn.list_execution_blocks()
            for eb_id in eb_list:
                eb = txn.get_execution_block(eb_id)
                status = eb.get("status")
                assert status == "ACTIVE"

                pb_state = txn.get_processing_block_state(pb_id)
                pb_status = pb_state.get("status")
                assert pb_status == "RUNNING"

                work_phase.ee_deploy_helm(deploy_name)
                deployment_list = txn.list_deployments()
                assert deploy_id in deployment_list

                # Set processing block to READY
                work_phase.update_pb_state(status="READY")
                p_state = txn.get_processing_block_state(pb_id)
                p_status = p_state.get("status")
                assert p_status == "READY"

                # Set execution block to FINISHED
                eb = {"subarray_id": None, "status": "FINISHED"}
                eb_state = txn.get_execution_block(eb_id)
                eb_state.update(eb)
                txn.update_execution_block(eb_id, eb_state)

                eb = txn.get_execution_block(eb_id)
                status = eb.get("status")
                assert status == "FINISHED"

    for txn in CONFIG_DB_CLIENT.txn():
        pb_state = txn.get_processing_block_state(pb_id)
        pb_status = pb_state.get("status")
        assert pb_status == "FINISHED"


@patch.dict(os.environ, MOCK_ENV_VARS)
def test_batch_script():
    """Test batch script"""

    def calc(x, y):
        x1 = x
        y1 = y
        z = x1 + y1
        return z

    # Wipe the config DB
    wipe_config_db()

    # Create eb and pb
    create_eb_pb()

    # Create processing block states
    create_pb_states()

    pb_id = "pb-test-20200425-00002"
    deploy_name = "dask"
    # deploy_id = 'proc-{}-{}'.format(pb_id, deploy_name)
    n_workers = 2
    work_phase = create_work_phase(pb_id)

    with work_phase:
        for txn in CONFIG_DB_CLIENT.txn():
            pb_state = txn.get_processing_block_state(pb_id)
            pb_status = pb_state.get("status")
            assert pb_status == "RUNNING"

        deploy = work_phase.ee_deploy_dask(
            deploy_name, n_workers, calc, (1, 5)
        )

        for txn in CONFIG_DB_CLIENT.txn():
            deploy_id = deploy.get_id()
            if deploy_id is not None:
                deployment_list = txn.list_deployments()
                assert deploy_id in deployment_list
                break
            txn.loop(wait=True)

        for txn in CONFIG_DB_CLIENT.txn():
            state = txn.get_processing_block_state(pb_id)
            deployments = state.get("deployments")
            deployments[deploy_id] = "FINISHED"
            state["deployments"] = deployments
            txn.update_processing_block_state(pb_id, state)

    for txn in CONFIG_DB_CLIENT.txn():
        pb_state = txn.get_processing_block_state(pb_id)
        pb_status = pb_state.get("status")
        assert pb_status == "FINISHED"


@patch.dict(os.environ, MOCK_ENV_VARS)
def test_receive_addresses_set_correctly():
    """
    Receive addresses are set correctly using information
    from the configuration string.
    """
    wipe_config_db()
    create_eb_pb()
    create_pb_states()

    pb_id = "pb-test-20200425-00000"
    pb = ProcessingBlock(pb_id)
    host_port, _ = pb.configure_recv_processes_ports(
        pb.get_scan_types(), 4, 9000, 1
    )

    # this updates the sate of the processing block
    # with the receive addresses
    pb.receive_addresses(
        host_port, chart_name="proc-pb-test-20200425-00000-test-receive"
    )

    for txn in CONFIG_DB_CLIENT.txn():
        # get receive addresses back from the pb state
        pb_state = txn.get_processing_block_state(pb_id)
        recv_address = pb_state.get("receive_addresses")

        assert recv_address == RECV_ADDR_FROM_CONFIG_STRING


@patch.dict(os.environ, MOCK_ENV_VARS)
def test_receive_addresses_multiple_addresses():
    """
    Test generating and updating receive addresses,
    for multiple channels.

    Scenario: two channels with maximum channels
              per receiver set to 10
    """

    # Wipe the config DB
    wipe_config_db()

    # Create eb and pb
    create_eb_pb()

    # Create processing block states
    create_pb_states()

    pb_id = "pb-test-20200425-00000"
    pb = ProcessingBlock(pb_id)

    work_phase = pb.create_phase("Work", [])

    with work_phase:
        for txn in CONFIG_DB_CLIENT.txn():
            eb_list = txn.list_execution_blocks()
            for eb_id in eb_list:
                work_phase.ee_deploy_helm(
                    "test-receive", pb.nested_parameters(VALUES)
                )
                host_port, num_process = pb.configure_recv_processes_ports(
                    SCAN_TYPES, 10, 9000, 1
                )

                VALUES["replicas"] = num_process
                pb.receive_addresses(host_port)
                pb_state = txn.get_processing_block_state(pb_id)
                recv_address = pb_state.get("receive_addresses")
                assert recv_address == RECV_ADDRESS

                # Set processing block state to READY
                work_phase.update_pb_state(status="READY")
                p_state = txn.get_processing_block_state(pb_id)
                p_status = p_state.get("status")
                assert p_status == "READY"

                # Set execution block to FINISHED
                eb = {"subarray_id": None, "status": "FINISHED"}
                eb_state = txn.get_execution_block(eb_id)
                eb_state.update(eb)
                txn.update_execution_block(eb_id, eb_state)

    for txn in CONFIG_DB_CLIENT.txn():
        pb_state = txn.get_processing_block_state(pb_id)
        pb_status = pb_state.get("status")
        assert pb_status == "FINISHED"


@patch.dict(os.environ, MOCK_ENV_VARS)
def test_dns_name():
    """Test generating dns name."""

    # Wipe the config DB
    wipe_config_db()

    # Create eb and pb
    create_eb_pb()

    # Create processing block states
    create_pb_states()

    pb_id = "pb-test-20200425-00000"
    pb = ProcessingBlock(pb_id)

    # Port and receive process configuration
    host_port, num_process = pb.configure_recv_processes_ports(
        pb.get_scan_types(), 10, 9000, 1
    )
    VALUES["replicas"] = num_process

    work_phase = pb.create_phase("Work", [])
    expected_dns_name = [
        "test-recv-0.receive.test-sdp.svc.cluster.local",
        "proc-pb-test-20200425-00000-test-receive-0."
        "receive.sdp.svc.cluster.local",
    ]

    with work_phase:
        for txn in CONFIG_DB_CLIENT.txn():

            for eb_id in txn.list_execution_blocks():
                ee_receive = work_phase.ee_deploy_helm("test-receive")

                # Testing with just passing scan types
                pb.receive_addresses(
                    host_port,
                    chart_name=ee_receive.get_id(),
                )
                state = txn.get_processing_block_state(pb_id)
                pb_recv_addresses = state.get("receive_addresses")
                pb_science_host = pb_recv_addresses["target:a"]["vis0"].get(
                    "host"
                )
                assert pb_science_host[0][1] == expected_dns_name[1]

                # Testing with statefulset name, service name and namespace
                host_port1, num_process1 = pb.configure_recv_processes_ports(
                    SCAN_TYPES, 10, 9000, 1
                )
                VALUES["replicas"] = num_process1
                pb.receive_addresses(
                    host_port1, "test-recv", "receive", "test-sdp"
                )
                state = txn.get_processing_block_state(pb_id)
                pb_receive_addresses = state.get("receive_addresses")
                pb_cal_host = pb_receive_addresses["target:b"]["vis0"].get(
                    "host"
                )
                assert pb_cal_host[0][1] == expected_dns_name[0]

                # Set processing block state to READY
                work_phase.update_pb_state(status="READY")
                p_state = txn.get_processing_block_state(pb_id)
                p_status = p_state.get("status")
                assert p_status == "READY"

                # Set execution block to FINISHED
                eb_state = txn.get_execution_block(eb_id)
                eb_state.update({"subarray_id": None, "status": "FINISHED"})
                txn.update_execution_block(eb_id, eb_state)

    for txn in CONFIG_DB_CLIENT.txn():
        pb_state = txn.get_processing_block_state(pb_id)
        assert pb_state.get("status") == "FINISHED"


@patch.dict(os.environ, MOCK_ENV_VARS)
def test_port():
    """Test generating and updating receive addresses."""

    # Wipe the config DB
    wipe_config_db()

    # Create eb and pb
    create_eb_pb()

    # Create processing block states
    create_pb_states()

    pb_id = "pb-test-20200425-00000"
    pb = ProcessingBlock(pb_id)

    # Port and receive process configuration
    host_port, num_process = pb.configure_recv_processes_ports(
        SCAN_TYPES, 10, 41000, 2
    )

    work_phase = pb.create_phase("Work", [])

    VALUES["num_process"] = num_process
    assert VALUES["num_process"] == 2

    # Get the expected receive addresses from the data file
    receive_addresses_expected = read_json_data(
        "receive_addresses_multiple_ports.json", decode=True
    )

    with work_phase:
        for txn in CONFIG_DB_CLIENT.txn():
            eb_list = txn.list_execution_blocks()
            for eb_id in eb_list:

                work_phase.ee_deploy_helm(
                    "test-receive", pb.nested_parameters(VALUES)
                )
                pb.receive_addresses(host_port)
                state = txn.get_processing_block_state(pb_id)
                pb_receive_addresses = state.get("receive_addresses")
                assert pb_receive_addresses == receive_addresses_expected

                # Set processing block state to READY
                work_phase.update_pb_state(status="READY")
                p_state = txn.get_processing_block_state(pb_id)
                p_status = p_state.get("status")
                assert p_status == "READY"

                # Set execution block to FINISHED
                eb = {"subarray_id": None, "status": "FINISHED"}
                eb_state = txn.get_execution_block(eb_id)
                eb_state.update(eb)
                txn.update_execution_block(eb_id, eb_state)

    for txn in CONFIG_DB_CLIENT.txn():
        pb_state = txn.get_processing_block_state(pb_id)
        pb_status = pb_state.get("status")
        assert pb_status == "FINISHED"


def test_get_scan_types():
    """
    Test that scan_types are correctly updated using
    default_scan_types and channel information
    """
    wipe_config_db()
    create_eb_pb()

    config = read_configuration_string()
    channels = config.get("channels")

    # check that the scan_type which derives from a pseudo (default) scan type
    # doesn't yet contain the channel information from the default scan type
    default_scan_type = config.get("scan_types")[0]
    original_scan_type = config.get("scan_types")[1]
    assert original_scan_type.get("scan_type_id") == "target:a"
    assert original_scan_type.get("beams") != default_scan_type.get("beams")

    # the beam called 'vis0' in target:0 will be updated with
    # channel information and the information from the default_scan_type
    # for vis0 it will also have it's original information
    # NOTE: these are matched based on criteria (see code),
    # not all beam types will have all the channels
    expected_vis0_beam = {
        **original_scan_type.get("beams")["vis0"],
        **default_scan_type.get("beams")["vis0"],
        **channels[0],
    }

    pb_id = "pb-test-20200425-00000"
    pb = ProcessingBlock(pb_id)

    # now we update scan_type target:a
    result_scan_types = pb.get_scan_types()

    assert (
        len(result_scan_types) == 1
    )  # we removed the default scan type and updated the original one
    assert result_scan_types[0].get("scan_type_id") == "target:a"
    assert result_scan_types[0].get("beams")["vis0"] == expected_vis0_beam


def test_configure_recv_processes_ports():
    """
    Test that a correct dictionary is configured for receive addresses,
    which is updated with port and function and provides an initial
    list of hosts.
    """
    wipe_config_db()
    create_eb_pb()

    expected_recv_dict = {
        "target:a": {
            "vis0": {
                "host": [[0, "-0."], [2000, "-1."], [2004, "-2."]],
                "port": [[0, 9000, 1], [2000, 9000, 1], [2004, 9000, 1]],
            }
        },
        "target:b": {
            "vis0": {
                "host": [[0, "-0."], [2000, "-1."]],
                "port": [[0, 9000, 1], [2000, 9000, 1]],
            }
        },
    }

    pb_id = "pb-test-20200425-00000"
    pb = ProcessingBlock(pb_id)

    result_recv_dict, result_num_proc = pb.configure_recv_processes_ports(
        SCAN_TYPES, 4, 9000, 1
    )

    assert result_num_proc == 2
    assert result_recv_dict == expected_recv_dict


def test_add_config_beam_info_to_recv_addrs():
    """
    Test that "function" is added correctly to beams in
    receive addresses.

    It also adds {x}_beam_id to receive_address if present
    in the configuration string,
    which in the test case is present for the pss1 beam.
    The key for this is "search_beam_id".

    Depending on beam type, we get the correct
    "function" and "{x}_beam_key" added.
    """
    wipe_config_db()
    create_eb_pb()

    pb_id = "pb-test-20200425-00000"
    pb = ProcessingBlock(pb_id)

    receive_addresses = deepcopy(RECV_ADDR_FROM_CONFIG_STRING)
    del receive_addresses["target:a"]["vis0"]["function"]
    del receive_addresses["target:a"]["pss1"]["function"]
    del receive_addresses["target:a"]["pss1"]["search_beam_id"]
    assert receive_addresses != RECV_ADDR_FROM_CONFIG_STRING

    # pylint: disable=protected-access
    pb._add_config_beam_info_to_recv_addrs(receive_addresses)

    assert receive_addresses == RECV_ADDR_FROM_CONFIG_STRING


# -----------------------------------------------------------------------------
# Ancillary functions
# -----------------------------------------------------------------------------


def wipe_config_db():
    """Remove all entries in the config DB."""
    CONFIG_DB_CLIENT.backend.delete("/pb", must_exist=False, recursive=True)
    CONFIG_DB_CLIENT.backend.delete("/eb", must_exist=False, recursive=True)
    CONFIG_DB_CLIENT.backend.delete(
        "/deploy", must_exist=False, recursive=True
    )


def create_work_phase(pb_id):
    """Create work phase."""
    pb = ProcessingBlock(pb_id)
    in_buffer_res = pb.request_buffer(100e6, tags=["sdm"])
    out_buffer_res = pb.request_buffer(10 * 6e15 / 3600, tags=["visibilities"])
    work_phase = pb.create_phase("Work", [in_buffer_res, out_buffer_res])

    return work_phase


def create_eb_pb():
    """Create execution block and processing block."""
    eb, pbs = get_eb_pbs()
    for txn in CONFIG_DB_CLIENT.txn():
        eb_id = eb.get("eb_id")
        if eb_id is not None:
            txn.create_execution_block(eb_id, eb)
        for pb in pbs:
            txn.create_processing_block(pb)


def get_eb_pbs():
    """Get EB and PBs from configuration string."""
    config = read_configuration_string()

    pbs_from_config = config.pop("processing_blocks")
    eb_id = config.get("eb_id")

    eb_config = config.copy()
    eb_extra = {
        "subarray_id": SUBARRAY_ID,
        "pb_realtime": [],
        "pb_batch": [],
        "pb_receive_addresses": None,
        "current_scan_type": None,
        "scan_id": None,
        "status": "ACTIVE",
    }
    eb = {**eb_extra, **eb_config}

    pbs = []
    for pbc in pbs_from_config:
        pb_id = pbc.get("pb_id")
        kind = pbc.get("script").get("kind")
        eb["pb_" + kind].append(pb_id)
        if "dependencies" in pbc:
            dependencies = pbc.get("dependencies")
        else:
            dependencies = []
        pb = ska_sdp_config.ProcessingBlock(
            pb_id,
            eb_id,
            pbc.get("script"),
            parameters=pbc.get("parameters"),
            dependencies=dependencies,
        )
        pbs.append(pb)

    return eb, pbs


def create_pb_states():
    """Create PB states in the config DB.

    This creates the PB states with status = RUNNING, and for any script
    matching the list of receive scripts, it adds the receive addresses.

    """

    for txn in CONFIG_DB_CLIENT.txn():
        pb_list = txn.list_processing_blocks()
        for pb_id in pb_list:
            pb_state = txn.get_processing_block_state(pb_id)
            if pb_state is None:
                pb_state = {"status": "RUNNING"}
                txn.create_processing_block_state(pb_id, pb_state)


def read_configuration_string():
    """Read configuration string from JSON file."""
    return read_json_data("configuration_string.json", decode=True)


def read_parameters(filename):
    """Read parameters from yaml file.

    :param filename: values filename

    """
    path = os.path.join(os.path.dirname(__file__), "data", filename)
    with open(path, "r", encoding="utf8") as data:
        expected_yaml = yaml.load(data)
    return expected_yaml


def read_json_data(filename, decode=False):
    """Read JSON file from data directory.

    :param decode: decode the JSON dat into Python

    """
    path = os.path.join(os.path.dirname(__file__), "data", filename)
    with open(path, "r", encoding="utf8") as file:
        data = file.read()
    if decode:
        data = json.loads(data)
    return data
