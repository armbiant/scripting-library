Functionality
=============

The required functionality of the scripting library is as follows.

Starting, monitoring and ending a script
----------------------------------------

At the start
^^^^^^^^^^^^

  - Claim the processing block.
  - Get the parameters defined in the processing block. They should be checked
    against the parameter schema defined for the script.

Resource requests
^^^^^^^^^^^^^^^^^

  - Make requests for input and output buffer space. The script will
    calculate the resources it needs based on the parameters, then request them
    from the processing controller. This is currently a placeholder.

Declare script phases
^^^^^^^^^^^^^^^^^^^^^

  - Scripts will be divided into phases such as preparation, processing,
    and clean-up. In the current implementation, only one phase can be
    declared, which we refer to as the 'work' phase.

Execute the work phase
^^^^^^^^^^^^^^^^^^^^^^

  - On entry to the work phase, it waits until the resources are available.
    Meanwhile it monitors the processing block to see it has been cancelled.
    For real-time scripts, it also checks if the execution block has been
    cancelled.
  - Deploy execution engines to execute a script/function.
  - Monitor the execution engines and processing block state. Waits until the
    execution is finished, or the processing block is cancelled.

At the end
^^^^^^^^^^

  - Remove the execution engines to release the resources.
  - Update processing block state with information about the success or failure
    of the script.

Receive scripts
---------------

 - Get IP and MAC addresses for the receive processes.
 - Monitor receive processes. If any get restarted, then the addresses may need to be updated.
 - Write the addresses in the appropriate format into the processing block state.

Compatibility with the telescope model library
----------------------------------------------

We keep the scripting library compatible with the latest version
of the `telescope model library <https://gitlab.com/ska-telescope/ska-telmodel>`_.
If you use a configuration string that is based on an older version
of the telescope model, you may experience errors or unexpected behaviour.
