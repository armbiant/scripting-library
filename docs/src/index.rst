SDP Scripting Library
=====================

The SDP scripting library is a high-level interface for writing processing
scripts. Its goal is to provide abstractions to enable the developer to express
the high-level organisation of a processing script without needing to interact
directly with the low-level interfaces such as the SDP configuration library.

.. toctree::
  :maxdepth: 1

  development
  functionality
  port_configuration
  api

Indices and tables
------------------

- :ref:`genindex`

