Development
===========

Installation
------------

The library can be installed using ``pip`` but you need to make sure to use the
SKA artefact repository as the index:

.. code-block::

  pip install \
    --index-url https://artefact.skao.int/repository/pypi-all/simple \
    ska-sdp-scripting

To install it using a ``requirements.txt`` file, the ``pip`` options can be
added to the top of the file like this:

.. code-block::

  --index-url https://artefact.skao.int/repository/pypi-all/simple
  ska-sdp-scripting

Usage
-----

Once the SDP scripting library has been installed, use:

.. code-block::

  import ska_sdp_scripting

Develop a new script
--------------------

The steps to develop and test an SDP processing script can be found at
`Script Development <https://developer.skao.int/projects/ska-sdp-script/en/latest/script-development.html>`_.
